#DATABASE
def ulang():
	print()
	print("Coba lagi ? [Y/N]")
	back = input().upper()
	if back ==("Y"):
		import os
		os.system('cls')
		menu()
	else:
		import os
		os.system('cls')
		exit()

def persegi():
	import os
	os.system('cls')
	print()
	print("Menghitung Luas Persegi")
	print()
	s = float(input("Masukkan panjang sisi (cm) = "))
	luas = (s*s)
	print()
	print("Luasnya adalah",luas,"cm2")
	print()
	ulang()

def perpan():
	import os
	os.system('cls')
	print()
	print("Menghitung Luas Persegi Panjang")
	print()
	p = float(input("Masukkan panjang (cm) = "))
	l = float(input("Masukkan lebar (cm) = "))
	luas = (p*l)
	print()
	print("Luasnya adalah",luas,"cm2")
	print()
	ulang()

def segitiga():
	import os
	os.system('cls')
	print()
	print("Menghitung Luas Segitiga")
	print()
	a = float(input("Masukkan alas (cm) = "))
	t = float(input("Masukkan tinggi (cm) = "))
	luas = (1/2*a*t)
	print()
	print("Luasnya adalah",luas,"cm2")
	print()
	ulang()

def lingkaran():
	import os
	os.system('cls')
	print()
	print("Menghitung Luas Lingkaran")
	print()
	r = float(input("Masukkan jari-jari (cm) = "))
	luas = (3.14*r*r)
	print()
	print("Luasnya adalah",luas,"cm2")
	print()
	ulang()

def jagen():
	import os
	os.system('cls')
	print()
	print("Menghitung Luas Jajar Genjang")
	print()
	a = float(input("Masukkan alas (cm) = "))
	t = float(input("Masukkan tinggi (cm) = "))
	luas = (a*t)
	print()
	print("Luasnya adalah",luas,"cm2")
	print()
	ulang()

def layang():
	import os
	os.system('cls')
	print()
	print("Menghitung Luas Layang - Layang")
	print()
	d1 = float(input("Masukkan diagonal 1 (cm) = "))
	d2 = float(input("Masukkan diagonal 2 (cm) = "))
	luas = (1/2*d1*d2)
	print()
	print("Luasnya adalah",luas,"cm2")
	print()
	ulang()

def trapesium():
	import os
	os.system('cls')
	print()
	print("Menghitung Luas Trapesium")
	print()
	a = float(input("Masukkan alas (cm) = "))
	c = float(input("Masukkan sisi yang sejajar dengan alas (cm) = "))
	t = float(input("Masukkan tinggi (cm) = "))
	luas = (1/2*(a+c)*t)
	print()
	print("Luasnya adalah",luas,"cm2")
	print()
	ulang()

def belke():
	import os
	os.system('cls')
	print()
	print("Menghitung Luas Belah Ketupat")
	print()
	d1 = float(input("Masukkan diagonal 1 (cm) = "))
	d2 = float(input("Masukkan diagonal 2 (cm) = "))
	luas = (1/2*d1*d2)
	print()
	print("Luasnya adalah",luas,"cm2")
	print()
	ulang()

def menu():
	print()
	print("Pilih Bentuk Bangun Datar :")
	print()
	print("1. Persegi")
	print("2. Persegi Panjang")
	print("3. Segitiga")
	print("4. Lingkaran")
	print("5. Jajar Genjang")
	print("6. Layang - Layang")
	print("7. Trapesium")
	print("8. Belah Ketupat")
	print()
	pilih = input("Masukan Pilihan : ")
	if(pilih=="1"):
		persegi()
	elif(pilih=="2"):
		perpan()
	elif(pilih=="3"):
		segitiga()
	elif(pilih=="4"):
		lingkaran()
	elif(pilih=="5"):
		jagen()
	elif(pilih=="6"):
		layang()
	elif(pilih=="7"):
		trapesium()
	elif(pilih=="8"):
		belke()
	else:
		import os
		os.system('cls')
		print()
		print("Maaf pilihan yang anda masukkan tidak terdaftar")
		ulang()

#START
import os
os.system('title Penghitung Luas Bangun Datar')
menu()